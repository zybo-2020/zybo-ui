EXE=t5
CC=gcc

$(EXE): 
	$(CC) -o $(EXE) $(EXE).c

driver:
	$(CC) -o driver driver.c

.PHONY: clean

clean:
	rm $(EXE)